package main

import "testing"

//Minimum Transaction Fee: 0.0001 VRC (Paid to Stakers)
//Confirmations: 10, Maturity: 500
var VeriCoin = NewClient("http://a:b@localhost:14022/", 10);


func TestSetFee(t *testing.T) {

	resp, err := VeriCoin.SetFee(0.0001)
  if err != nil {
     t.Errorf("setFee error: %+v", err)
     t.FailNow()
  }
  t.Logf("setFee result: %v", resp)
}

func TestCreateAddress(t *testing.T) {

	resp, err := VeriCoin.CreateAddress()
	if err != nil {
		 t.Errorf("createAddress error: %+v", err)
		 t.FailNow()
	}
	t.Logf("createAddress result: %v", resp)
}

func TestGetBalance(t *testing.T) {

  resp, err := VeriCoin.GetBalance()
  if err != nil {
     t.Errorf("getBalance error: %+v", err)
     t.FailNow()
  }
  t.Logf("getBalance result: %v", resp)
}

func TestGetWalletInfo(t *testing.T) {
  resp, err := VeriCoin.GetWalletInfo()
  if err != nil {
     t.Errorf("getWalletInfo error: %+v", err)
     t.FailNow()
  }
  t.Logf("getWalletInfo result: %+v", resp)
}

func TestGetBalanceByAddress(t *testing.T) {
  resp, err := VeriCoin.GetBalanceByAddress("VDuYVjKoTadioUL4BszjwVi1zvuSrLX6ex")
  if err != nil {
     t.Errorf("getBalanceByAddress error: %+v", err)
     t.FailNow()
  }
  t.Logf("getBalanceByAddress result: %v", resp)
}






func TestWalletLock(t *testing.T) {
  resp, err := VeriCoin.WalletLock()
  if err != nil {
     t.Errorf("WalletLock error: %+v", err)
     t.FailNow()
  }
  t.Logf("WalletLock result: %v", resp)
}


func TestUnlockWallet(t *testing.T) {
  resp, err := VeriCoin.UnlockWallet("sad9as7d8sa6dsa76d", 10)
  if err != nil {
     t.Errorf("WalletPassphrase error: %+v", err)
     t.FailNow()
  }
  t.Logf("WalletPassphrase result: %v", resp)
}


func TestSendToAddress(t *testing.T) {

  addr := "VAhrtcY6LKdhPFcxnn3hs1SJVsvyHTEpXD"
  resp, err := VeriCoin.SendToAddress(addr, 0.00001)
  if err != nil {
     t.Errorf("sendToAddress error: %+v", err)
     t.FailNow()
  }
  t.Logf("sendToAddress result: %v", resp)
}



func TestGetTransaction(t *testing.T) {
  resp, err := VeriCoin.GetTransaction("4a280b9350a397729ab255dfc50fd24f679ca725caf6c45275cf37ce74321e42")
  if err != nil {
     t.Errorf("getTransaction error: %+v", err)
     t.FailNow()
  }
  t.Logf("getTransaction result: %v", resp)
}

func TestCheckTransaction(t *testing.T) {
  resp, err := VeriCoin.CheckTransaction("4a280b9350a397729ab255dfc50fd24f679ca725caf6c45275cf37ce74321e42")
  if err != nil {
     t.Errorf("getTransaction error: %+v", err)
     t.FailNow()
  }
  t.Logf("getTransaction result: %v", resp)
}
