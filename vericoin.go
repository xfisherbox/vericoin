package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"reflect"
	"strconv"
	"time"
)

type Client interface {

	SetFee(fee float64) (bool, error)
	CreateAddress() (string, error)
	GetBalance() (float64, error)
	GetWalletInfo() (*BtcWalletInfo, error)
	GetBalanceByAddress(address string) (float64, error)
	SendToAddress(address string, amount float64) (string, error)
	GetTransaction(txid string) (map[string]interface{}, error)
	CheckTransaction(txid string) (bool, error)
	UnlockWallet(passwd string, timeout int) (bool, error)
	WalletLock() (bool, error)

}

type RequestData struct {
	Jsonrpc string        `json:"jsonrpc"`
	Method  string        `json:"method"`
	Params  []interface{} `json:"params"`
}

type CoinClient struct {
	Url           string
	Confirmations int64
}

type BtcError struct {
	Code    int64  `json:"code"`
	Message string `json:"message"`
}

type BtcWalletInfo struct {
	Hdmasterkeyid         string  `json:"hdmasterkeyid"`
	Walletname            string  `json:"walletname"`
	Walletversion         string  `json:"walletversion"`
	Balance               float64 `json:"balance"`
	UnconfirmedBalance    float64 `json:"unconfirmed_balance"`
	Keypoololdest         float64 `json:"keypoololdest"`
	Keypoolsize           int64   `json:"keypoolsize"`
	ImmatureBalance       float64 `json:"immature_balance"`
	Txcount               int64   `json:"txcount"`
	KeypoolsizeHdInternal int64   `json:"keypoolsize_hd_internal"`
	Paytxfee              float64 `json:"paytxfee"`
}

func (e BtcError) Error() string {
	return strconv.FormatInt(e.Code, 10) + ": " + e.Message
}

func (e *BtcWalletInfo) fill(obj map[string]interface{}) error {
	fields := map[string]string{
		"hdmasterkeyid":           "Hdmasterkeyid",
		"walletname":              "Walletname",
		"walletversion":           "Walletversion",
		"balance":                 "Balance",
		"unconfirmed_balance":     "UnconfirmedBalance",
		"keypoololdest":           "Keypoololdest",
		"keypoolsize":             "Keypoolsize",
		"immature_balance":        "ImmatureBalance",
		"txcount":                 "Txcount",
		"keypoolsize_hd_internal": "KeypoolsizeHdInternal",
		"paytxfee":                "Paytxfee",
	}
	for k, v := range fields {
		if val, ok := obj[k]; ok {
			field := reflect.ValueOf(e).Elem().FieldByName(v)
			if !field.IsValid() {
				continue
			}
			valtype := reflect.TypeOf(val).Name()
			switch field.Type().Name() {
			case "string":
				valueString := ""
				if valtype == "string" {
					valueString, _ = val.(string)
				} else {
					valueString = strconv.FormatFloat(val.(float64), 'f', -1, 64)
				}
				field.SetString(valueString)
			case "float64":
				var valueFloat float64
				if valtype == "string" {
					valueFloat, _ = strconv.ParseFloat(val.(string), 64)
				} else {
					valueFloat = val.(float64)
				}
				field.SetFloat(valueFloat)
			case "int64":
				var valueInt int64
				if valtype == "string" {
					valueInt, _ = strconv.ParseInt(val.(string), 10, 64)
				} else {
					valueInt = int64(val.(float64))
				}
				field.SetInt(valueInt)
			}
		}
	}

	return nil
}


func checkBtcError(obj map[string]interface{}) error {
	if val, ok := obj["error"]; ok {
		if error, ok := val.(map[string]interface{}); ok {
			ecode := int64(0)
			emsg := ""
			if val, ok = error["code"]; ok {
				if reflect.TypeOf(val).Name() == "string" {
					ecode, _ = strconv.ParseInt(val.(string), 10, 64)
				} else {
					ecode = int64(val.(float64))
				}
			}
			if val, ok = error["message"]; ok {
				emsg = val.(string)
			}
			if ecode != 0 {
				res := BtcError{Code: ecode, Message: emsg}
				return res
			}
		}
	}
	return nil
}

func (b *CoinClient) sendRequest(reqbody []byte) (map[string]interface{}, error) {
	req, err := http.NewRequest("POST", b.Url, bytes.NewBuffer(reqbody))
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 30 * time.Second}
	resp, err := client.Do(req)
	var body []byte

	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var res_dat map[string]interface{}
	if err = json.Unmarshal(body, &res_dat); err != nil {
		return nil, err
	}

	response := checkBtcError(res_dat)
	if response != nil {

		return nil, response
	}

	return res_dat, nil
}

func NewClient(url string, confirmations int64) Client {
	return &CoinClient{Url: url, Confirmations: confirmations}
}

func (b *CoinClient) CreateAddress() (string, error) {
	req, e := json.Marshal(RequestData{Jsonrpc: "2.0", Method: "getnewaddress"})
	if e != nil {
		return "", e
	}

	resp, err := b.sendRequest(req)
	if err != nil {
		return "", err
	}
	if val, ok := resp["result"]; ok {
		if res, ok := val.(string); ok {
			return res, nil
		}
	}

	return "", BtcError{Code: 500, Message: "No result"}
}

func (b *CoinClient) GetBalance() (float64, error) {
	req, e := json.Marshal(RequestData{Jsonrpc: "2.0", Method: "getbalance"})
	if e != nil {
		return 0, e
	}

	resp, err := b.sendRequest(req)
	if err != nil {
		return 0, err
	}
	if val, ok := resp["result"]; ok {
		var res float64
		if reflect.TypeOf(val).Name() == "string" {
			res, _ = strconv.ParseFloat(val.(string), 64)
		} else {
			res = val.(float64)
		}
		return res, nil
	}

	return 0, BtcError{Code: 500, Message: "No result"}
}

func (b *CoinClient) GetWalletInfo() (*BtcWalletInfo, error) {
	req, e := json.Marshal(RequestData{Jsonrpc: "2.0", Method: "getinfo"})
	if e != nil {
		return nil, e
	}

	resp, err := b.sendRequest(req)
	if err != nil {
		return nil, err
	}
	if val, ok := resp["result"]; ok {
		if result, ok := val.(map[string]interface{}); ok {
			res_obj := new(BtcWalletInfo)
			res_obj.fill(result)
			return res_obj, nil
		}
	}

	return nil, BtcError{Code: 500, Message: "No result"}
}


func (b *CoinClient) CheckTransaction(txid string) (bool, error) {
	params := []interface{}{txid}
	req, e := json.Marshal(RequestData{Jsonrpc: "2.0", Method: "gettransaction", Params: params})
	if e != nil {
		return false, e
	}

	resp, err := b.sendRequest(req)
	if err != nil {
		return false, err
	}
	if val, ok := resp["result"]; ok {
		if result, ok := val.(map[string]interface{}); ok {
			//log.Println("transaction result", result)
			if val, ok := result["confirmations"]; ok {
				var res int64
				if reflect.TypeOf(val).Name() == "string" {
					res, _ = strconv.ParseInt(val.(string), 10, 64)
				} else {
					res = int64(val.(float64))
				}
				return res >= b.Confirmations, nil
			}
		}
	}

	return false, BtcError{Code: 500, Message: "No result"}
}


func (b *CoinClient) GetBalanceByAddress(address string) (float64, error) {
	type BalanceRequestData struct {
		Jsonrpc string        `json:"jsonrpc"`
		Method  string        `json:"method"`
		Params  []interface{} `json:"params"`
	}
	params := []interface{}{}
	params = append(params, address, b.Confirmations)
	req, e := json.Marshal(BalanceRequestData{Jsonrpc: "2.0", Method: "getreceivedbyaddress", Params: params})
	//req, e := json.Marshal(BalanceRequestData{Jsonrpc: "2.0", Method: "getbalance", Params: params})
	if e != nil {
		return 0, e
	}

	resp, err := b.sendRequest(req)
	if err != nil {
		return 0, err
	}
	if val, ok := resp["result"]; ok {
		var res float64
		if reflect.TypeOf(val).Name() == "string" {
			res, _ = strconv.ParseFloat(val.(string), 64)
		} else {
			res = val.(float64)
		}
		return res, nil
	}

	return 0, BtcError{Code: 500, Message: "No result"}
}

func (b *CoinClient) SendToAddress(address string, amount float64) (string, error) {

	//params := []interface{}{address, strconv.FormatFloat(amount, 'f', -1, 64)}
	params := []interface{}{address, amount}

	req, e := json.Marshal(RequestData{Jsonrpc: "2.0", Method: "sendtoaddress", Params: params})
	if e != nil {
		return "", e
	}
	//log.Println("request", string(req))
	resp, err := b.sendRequest(req)
	if err != nil {
		return "", err
	}
	if val, ok := resp["result"]; ok {
		if res, ok := val.(string); ok {
			return res, nil
		}
	}

	return "", BtcError{Code: 500, Message: "No result"}

}

func (b *CoinClient) GetTransaction(txid string) (map[string]interface{}, error) {
	params := []interface{}{txid}
	req, e := json.Marshal(RequestData{Jsonrpc: "2.0", Method: "gettransaction", Params: params})
	if e != nil {
		return nil, e
	}

	resp, err := b.sendRequest(req)
	if err != nil {
		return nil, err
	}

	result := map[string]interface{}{}
	if val, ok := resp["result"]; ok {
		result = val.(map[string]interface{})
	}
	return result, nil
}

func (b *CoinClient) SetFee(fee float64) (bool, error) {
	params := []interface{}{fee}
	req, e := json.Marshal(RequestData{
		Jsonrpc: "2.0",
		Method:  "settxfee",
		Params:  params,
	})
	if e != nil {
		return false, e
	}
	//log.Println("request", string(req))

	resp, err := b.sendRequest(req)
	if err != nil {
		return false, err
	}
	if val, ok := resp["result"]; ok {
		if res, ok := val.(bool); ok {
			return res, nil
		}
	}

	return false, BtcError{Code: 500, Message: "No result"}
}


func (b *CoinClient) UnlockWallet(passwd string, timeout int) (bool, error) {

	params := []interface{}{passwd, timeout}
	req, e := json.Marshal(RequestData{Jsonrpc: "2.0", Method: "walletpassphrase", Params: params})
	if e != nil {
		return false, e
	}
	_, err := b.sendRequest(req)
	if err != nil {
		return false, err
	}
	return true, nil;

}

func (b *CoinClient) WalletLock() (bool, error) {

	req, e := json.Marshal(RequestData{Jsonrpc: "2.0", Method: "walletlock"})
	if e != nil {
		return false, e
	}
	_, err := b.sendRequest(req)
	if err != nil {
		return false, err
	}
	return true, nil;

}
